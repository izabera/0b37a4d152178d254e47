#include <iostream>
#include <cmath>
#include <fstream>
using namespace std;

string unita[10] = {"","uno","due","tre","quattro","cinque","sei","sette","otto","nove"};
string decine[10] = {"","dieci","venti","trenta","quaranta","cinquanta","sessanta","settanta","ottanta","novanta"};
string teen[10] = {"","undici","dodici","tredici","quattordici","quindici","sedici","diciassette","diciotto","diciannove"};

string una (int num) {
	return unita[num];
}

string due (int num) {
	if (num < 20 && num > 10) return teen[num-10];
	if (num < 10) return una(num);
	if (num % 10 == 1 || num % 10 == 8) return decine[num/10].substr(0,decine[num/10].size()-1)+unita[num%10];
	return decine[num/10]+una(num%10);
}

string tre (int num) {
	if (num < 100) return due(num);
	string cent = "cento";
	if (num >= 200) cent = unita[num/100]+"cento";
	return cent+due(num%100);
}

string quattro (int num) {
	if (num < 1000) return tre(num);
	string mill = "mille";
	if (num >= 2000) mill = tre(num/1000)+"mila";
	return mill+tre(num%1000);
}

string sette (int num) {
	if (num < 1000000) return quattro(num);
	string mili = "un milione";
	if (num >= 2000000) mili = tre(num/1000000)+"milioni";
	return mili+quattro(num%1000000);
}

void lettere (int num) {
	string negativo = "";
	if (num < 0) {
		negativo = "meno ";
		num *= -1;
	}
	if (num == 0) {
		cout << "zero" << endl;
		return;
	}
	cout << negativo << sette(num) << endl;
	return;
//funzia fino a |num|<1000000000
}

main (int argc, char** argv) {
	int num;
	if (argc > 1) {
		ifstream fin (argv[1]);
		while (fin >> num) lettere (num);
		fin.close();
	}
	else while (cin >> num) lettere (num);
	return 0;
}	
